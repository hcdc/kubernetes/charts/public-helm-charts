# Public Helm Charts of HCDC

This repository serves as an endpoint for public helm charts (currently, 
GitLab does not yet fully support helm chart package registries on a 
group level see [this issue on gitlab.com][issue]).

The packages in this registry can be seen in 
https://codebase.helmholtz.cloud/hcdc/kubernetes/charts/public-helm-charts/-/packages

## Channels

There are two channels available in this registry:

- `latest`: filled by the `main` branch of the projects
- `stable`: filled by releases

## Usage

To use helmcharts from this registry, run the following command:

```bash
helm repo add hcdc-public https://codebase.helmholtz.cloud/api/v4/projects/7205/packages/helm/<channel>
```

Replace the `<channel>` by the name of a channel from above (`latest` or `stable`)

## Uploading packages
To upload packages to this repository, use the following script:

```bash
curl --request POST \
     --form 'chart=@mychart-0.1.0.tgz' \
     --user <username>:<access_token> \
     https://codebase.helmholtz.cloud/api/v4/projects/7205/packages/helm/api/<channel>/charts
```

Replace the `<channel>` by the name of a channel from above (`latest` or `stable`)

[issue]: https://gitlab.com/gitlab-org/gitlab/-/issues/335928
